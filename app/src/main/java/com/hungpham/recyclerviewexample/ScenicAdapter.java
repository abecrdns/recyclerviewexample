package com.hungpham.recyclerviewexample;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hungpham.recyclerviewexample.databinding.ItemBinding;

import java.util.ArrayList;

public class ScenicAdapter extends RecyclerView.Adapter<ScenicAdapter.ViewHolder> {
    public ArrayList<Item> items;
    private final ClickListener clickListener;

    public ScenicAdapter(ArrayList<Item> items, ClickListener clickListener) {
        this.items = items;
        this.clickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemBinding binding = ItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding, clickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Item item = items.get(position);
        holder.bind(item);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ItemBinding binding;

        public ViewHolder(ItemBinding binding, ClickListener clickListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.getRoot().setOnClickListener(view ->
                    clickListener.onItemClick(getAdapterPosition())
            );
        }

        public void bind(Item item) {
            binding.name.setText(item.name);
            binding.background.setImageResource(item.backgroundResource);
        }
    }

    public interface ClickListener {
        void onItemClick(int position);
    }
}
